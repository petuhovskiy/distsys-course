#include <kv/types.hpp>
#include <kv/client/stub.hpp>
#include <kv/node/node.hpp>

// Simulation

#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/history/printers/kv.hpp>
#include <whirl/matrix/history/checker/check.hpp>
#include <whirl/matrix/history/models/kv.hpp>

#include <whirl/rpc/impl/id.hpp>
#include <await/fibers/core/id.hpp>

#include <random>

using namespace whirl;

//////////////////////////////////////////////////////////////////////

class KVClient : public ClientBase {
 public:
  KVClient(NodeServices services) : ClientBase(std::move(services)) {
  }

 protected:
  void MainThread() override {
    kv::KVBlockingStub kv_store{Channel()};

    for (size_t i = 1;; ++i) {
      if (RandomNumber() % 2 == 0) {
        kv::Value value = RandomNumber(1, 100);
        NODE_LOG("Execute Set({})", value);
        kv_store.Set("test", value);
        NODE_LOG("Set completed");
      } else {
        NODE_LOG("Execute Get(test)");
        kv::Value result = kv_store.Get("test");
        NODE_LOG("Get(test) -> {}", result);
      }

      Threads().SleepFor(RandomNumber(1, 100));
    }
  }
};

//////////////////////////////////////////////////////////////////////

using KVStoreModel = histories::KVStoreModel<kv::Key, kv::Value>;

//////////////////////////////////////////////////////////////////////

size_t NumberOfReplicas(size_t seed) {
  return 3 + seed % 5;
}

void RunSimulation(size_t seed) {
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Cluster nodes
  auto node = MakeNode<kv::KVNode>();
  world.AddServers(NumberOfReplicas(seed), node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(3, client);

  // Log
  std::stringstream log;
  world.WriteLogTo(log);

  world.Start();
  while (world.NumCompletedCalls() < 8) {
    world.Step();
  }
  world.Stop();

  const auto history = world.History();
  const bool linearizable = histories::LinCheck<KVStoreModel>(history);

  if (!linearizable) {
    std::cout << "Log:" << std::endl << log.rdbuf() << std::endl;
    fmt::print("History (seed = {}) is NOT LINEARIZABLE\n", seed);
    histories::PrintKVHistory<kv::Key, kv::Value>(history, std::cout);
    std::exit(1);
  }
}

//////////////////////////////////////////////////////////////////////

void RunSimulations(size_t iterations) {
  std::mt19937 seeds{42};

  for (size_t i = 1; i <= iterations; ++i) {
    std::cout << "Simulation " << i << "..." << std::endl;
    RunSimulation(seeds());
  }
}

//////////////////////////////////////////////////////////////////////

int main() {
  RunSimulations(123456);
  return 0;
}
