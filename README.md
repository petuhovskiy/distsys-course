# Distributed Systems Course

## Добро пожаловать!

[Настраиваем рабочее окружение](docs/setup.md)

## Библиотеки

* [Await](https://gitlab.com/Lipovsky/await) – Файберы, фьючи и комбинаторы
* [Whirl](https://gitlab.com/Lipovsky/whirl) – Детерминированный симулятор распределенной системы
