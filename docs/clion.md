# Работа в CLion

## Начальная настройка

### Шаг 1

Курс – это CMake-проект, так что просто откройте его в CLion: `File` > `Open...` > выбрать директорию локального репозитория курса.

### Шаг 2

В `Preferences` > `Build, Execution, Deployment` > `Toolchains` создайте Remote Host и заполните поля как на скриншоте:

![Setup remote host](images/toolchain.png)

![Setup remote host](images/credentials.png)

### Шаг 3

В `Preferences` > `Build, Execution, Deployment` > `CMake` добавьте новый профиль сборки и установите в нем созданный шагом ранее тулчейн

![Setup remote host](images/profile.png)

### Шаг 4

Готово! Теперь можно выбрать в IDE цель с задачей/тестами и запустить её!

## Полезные советы

- В любой непонятной ситуации с удаленной сборкой следует попробовать следующее заклинание:

ПКМ по корневой папке репозитория > `Deployment` > `Upload to...`

- В окошке `Terminal` можно залогиниться в контейнер и работать там с консольным клиентом